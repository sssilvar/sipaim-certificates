# SIPAIM Certificate generator
This allows to send automatically a bunhch of emails to every atendee to the conference.

It works on Linux and Python >= 3.6

## Requirements
The command `rsvg-convert` and Inkscape must be available.
To install them execute the following command:
* **Fedora:** `sudo dnf -y install librsvg2`
* **Ubuntu:** `sudo apt-get -y install librsvg2-bin`

## Instructions
There are three things you can change in the `generate_cert.py` script:

* `data_csv = 'db/Certificates_authors - Hoja 1.csv'` corresponds to the database of atendees, paper titles and e-mails.
* `svg_template = 'templates/certificate_authors.svg'` is the template designed in Inkscape of the certificate.



