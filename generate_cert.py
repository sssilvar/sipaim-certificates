#!/bin/env python3
import os
import smtplib
import unicodedata
from os.path import join, dirname, realpath, isfile, basename

import pandas as pd
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

root = dirname(realpath(__file__))

email_body = open(join(root, 'messages/msg.html')).read()

def strip_accents(text):
    """
    Strip accents from input String.

    :param text: The input string.
    :type text: String.

    :returns: The processed String.
    :rtype: String.
    """
    try:
        text = unicode(text, 'utf-8')
    except (TypeError, NameError): # unicode is a default on python 3 
        pass
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    text = text.lower().replace(' ', '_')
    return str(text)


def create_certificate(author_name, paper_title, template, out_folder='/tmp'):
    """
    Creates a certificate including author's name and paper's title.
    :param str author_name: Author's name
    :param str paper_title: Paper's title
    :param str template: SVG template file path
    :param str out_folder: Output folder
    :return:
    """
    # define template file
    base_filename = strip_accents(author_name) + '_cert.svg'
    out_svg = join(out_folder, base_filename)
    out_pdf = out_svg.replace('.svg', '.pdf')

    if isfile(out_pdf):
        "If Certificate was already generated."
        return out_pdf

    # Load template
    with open(svg_template) as template:
        template_data = template.read()

    # Replace info
    author_data = template_data.replace('>full_name</tspan>', f'>{author_name}</tspan>')
    author_data = author_data.replace('paper_title”.</tspan>', f'{paper_title}”.</tspan>')
    author_data = author_data.replace('paper_title”.</flowPara>', f'{paper_title}”.</flowPara>')

    with open(out_svg, 'w') as certificate:
        certificate.write(author_data)

    # Convert to PDF
    os.system(f'rsvg-convert -f pdf -o {out_pdf} {out_svg}')
    os.system(f'rm {out_svg}')
    return out_pdf


def send_email(email_to, attach_file):
    sender = 'info@sipaim.org'
    receivers = email_to

    msg = MIMEMultipart()
    msg['Subject'] = 'Certificate of attendance | SIPAIM 2019'
    msg['From'] = sender
    msg['To'] = email_to

    msg.attach(MIMEText(email_body, 'html'))
    attachment = MIMEBase('application', 'octet-stream')
    attachment.set_payload(open(attach_file, 'rb').read())
    encoders.encode_base64(attachment)
    attachment.add_header('Content-Disposition', f'attachment; filename="{basename(attach_file)}"')
    msg.attach(attachment)

    print('Connecting to SMTP server...')
    server = smtplib.SMTP_SSL('smtp.zoho.com', 465)
    print('Logging in...')
    server.login(user='info@sipaim.org', password='Cimalab.2019')

    print('Send email...')
    server.sendmail(sender, receivers, msg.as_string())
    server.close()


if __name__ == '__main__':
    # Define database of authors
    data_csv = '/home/ssilvari/Documents/SIPAIM2019/Certificates_posters - Hoja 1.csv'
    svg_template = 'templates/certificate_posters.svg'

    # Add root folder path
    data_csv = join(root, data_csv)
    svg_template = join(root, svg_template)
    print(f'Database CSV: {data_csv}')
    print(f'Certificate template: {data_csv}')

    # Read Database
    df = pd.read_csv(data_csv)

    # Generate certificates
    for index, row in df.iterrows():
        email_to = row['e-mail']
        full_name = row['full_name']
        paper_title = row['paper_title_1']

        if not pd.isna(full_name) and not pd.isna(email_to):
            print(f'Generating certificate for: {full_name}')
            pdf_cert = create_certificate(author_name=full_name,
                                          paper_title=paper_title,
                                          template=svg_template)

            print(f'Sending email to: {repr(email_to)} | Attachment: {pdf_cert}')
            send_email(email_to=email_to, attach_file=pdf_cert)
